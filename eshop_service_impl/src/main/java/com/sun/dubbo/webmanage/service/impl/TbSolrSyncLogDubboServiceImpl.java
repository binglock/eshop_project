package com.sun.dubbo.webmanage.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.commons.pojo.EasyUIDataGrid;
import com.sun.dubbo.webmanage.service.TbSolrSyncLogDubboService;
import com.sun.webmanage.mapper.TbSolrSyncLogMapper;
import com.sun.webmanage.model.TbSolrSyncLog;
import com.sun.webmanage.model.TbSolrSyncLogExample;

public class TbSolrSyncLogDubboServiceImpl implements TbSolrSyncLogDubboService{

	@Resource
	private TbSolrSyncLogMapper tbSolrSyncLogMapper;
	
	@Override
	public int insertSolrSyncLog(TbSolrSyncLog record) {
		return tbSolrSyncLogMapper.insertSelective(record);
	}

	@Override
	public EasyUIDataGrid selectSolrSyncLogList(int pager,int rows,Byte operator_type) {
		TbSolrSyncLogExample example = new TbSolrSyncLogExample();
		if(operator_type!=null){
		example.createCriteria().andOperaTypeEqualTo(operator_type);
		}
		example.setOrderByClause("created desc");
		PageHelper.startPage(pager, rows);
		List<TbSolrSyncLog> listdata = tbSolrSyncLogMapper.selectByExample(example);
		PageInfo<TbSolrSyncLog> page = new PageInfo<>(listdata);
		EasyUIDataGrid retdata = new EasyUIDataGrid();
		retdata.setRows(page.getList());
		retdata.setTotal(page.getTotal());
		return retdata;
	}

	@Override
	public List<TbSolrSyncLog> selectSolrSyncLogNewByType(byte operator_type) {
		TbSolrSyncLogExample example = new TbSolrSyncLogExample();
		example.createCriteria().andOperaTypeEqualTo(operator_type);
		example.setOrderByClause("created desc");
		PageHelper.startPage(1, 10);
		List<TbSolrSyncLog> listdata = tbSolrSyncLogMapper.selectByExample(example);
		PageInfo<TbSolrSyncLog> page = new PageInfo<>(listdata);
		return page.getList();
	}

}
