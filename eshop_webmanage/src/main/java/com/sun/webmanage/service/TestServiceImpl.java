package com.sun.webmanage.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.sun.dubbo.service.DubboTestService;
import com.sun.webmanage.model.TbContentCategory;

@Service("TestService")
public class TestServiceImpl implements TestService{
	
	@Reference
	private DubboTestService dubboTestServiceImpl;

	@Override
	public List<TbContentCategory> listData() {
		return dubboTestServiceImpl.getInfo();
	}
	
	

}
