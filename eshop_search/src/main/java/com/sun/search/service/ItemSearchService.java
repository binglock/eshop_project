package com.sun.search.service;

import java.io.IOException;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;

public interface ItemSearchService {

	Map<String,Object> tbItemSearch(int page,int rows,String query)throws SolrServerException, IOException ;
}
